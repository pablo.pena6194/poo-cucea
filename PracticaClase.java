// Programación orientada a objetos (Encabezado requerido en todos los programas de POO)
// Actividad 06:(Manejo de variables)
// By: PCPP

public class Actividad06 {
	
	public static void main (String[] args){
        double num1 = 3;
        double num2 = 5;

        //Sumar Valores
        System.out.println("El resultado de la suma es:  " + (num1 + num2));

        //Restar Valores
        System.out.println("El resultado de la resta es: " + (num1 - num2));

        //Multiplicar Valores
        System.out.println("El resultado de la multiplicacion es: " + (num1 * num2));

        //Dividir Valores
        System.out.println("El resultado de la division es: " + (num1 / num2));

        //Modulo de la division
        System.out.println("El resultado de la division(residuo) es: " + (num1 % num2));

        //Jerarquía de operadores
        System.out.println("El resultado de la expresión es: " + (num1 + num2 + 5 / 9));

        //Potencia
        System.out.println("El resultado de la Potencia es: " + Math.pow(num1, num2));

        

        }
        
      }